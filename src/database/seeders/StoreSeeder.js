const Store = require("../../models/Store");
const faker = require('faker-br');

const seedStore = async function () {
    try {
      await Store.sync({ force: true });
      const stores = [];
 
      for (let i = 0; i < 10; i++) {
 
       let store = await Store.create({
         name: faker.name.firstName(),
         email: faker.internet.email(),
         phone_number: faker.phone.phoneNumber(),
       })
    }
} catch (err) { console.log(err); }
}

module.exports = seedStore;