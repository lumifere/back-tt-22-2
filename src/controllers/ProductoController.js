const {Op} = require('sequelize');
const Producto = require('../models/Producto');
const User = require('../models/User')

const index = async(req, res) =>{
    try{
        const producto = await Producto.findAll();
        return res.status(200).json({producto});
    } catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const producto = await Producto.findByPk(id);
        return res.status(200).json({producto});
    }catch(err){
        return res.status(500).json({err});
    }
};
const create = async(req,res) => {
    try{
          const producto = await Producto.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: "Produto cadastrado com sucesso!", producto:producto});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Producto.update(req.body, {where: {id: id}});
        if(updated) {
            const producto = await Producto.findByPk(id);
            return res.status(200).send(producto);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Producto.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const purchase = async(req,res) => {
    const {productoId,userId } = req.params;
    try {
        const producto = await Producto.findByPk(productoId);
        const user = await User.findByPk(userId);
        await producto.setUser(user);
        return res.status(200).json({msg: "Compra concluída."});
    }catch(err){
        return res.status(500).json({err});
    }
}
const cancelPurchase = async(req,res) => {
    const {id} = req.params;
    try {
        const producto = await Producto.findByPk(id);
        await producto.setUser(null);
        return res.status(200).json({msg: "Compra cancelada."});
    }catch(err){
        return res.status(500).json({err});
    }
}
module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    purchase,
    cancelPurchase
}