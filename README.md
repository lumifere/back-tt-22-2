# <strong>Material das Aulas de Back-End TT 22.2

## :arrow_right: Introdução

Este repositório tem como objetivo manter todos os códigos feitos para o back-end, durante o processo seletivo 2022.2 da EJCM.

<br>
<hr>

## :arrow_right: Sumário

- Modelagem do Banco de Dados 
- Ferramentas Utilizadas
- Disposição e Download
- Execução
- Instrutores
<br>
<hr>
    
## :game_die: Modelagem do Banco de Dados ##

Modelagem do Banco de Dados feita no <a href="https://github.com/chcandido/brModelo">brModelo<a/>.
        
<div align="center">

![Database Modeling](https://i.imgur.com/ZJZAZhE.jpeg)  

</div>

<br>
<hr>

## :tools: Ferramentas Utilizadas

### Linguagem: 
JavaScript

### Tecnologias: 
**[Node.js](https://nodejs.org/en/)**  
**[Sequelize](https://sequelize.org/)**     
**[Express](http://expressjs.com/pt-br/)**

### Ferramenta de Versionamento: 
**[Git](https://git-scm.com/downloads)**

### Package Manager: 
**[NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)**  

<br>
<hr>

## :arrow_down: Disposição e Download

Os temas das aulas estão separados por branchs, sendo as aulas de Modelagem BD, Models I, Rotas & Controllers e Models II & Seeders estão na branch **main**. Todas as branchs utilizam o código da branch **main** como base.

Quando for pegar os códigos nesses repositório, você deve clonar o repositório já criando a sua branch específica, ou seja, caso vá dar aula de Middleware & Vallidation, você deve rodar o seguinte comando:

```git
git clone https://gitlab.com/lumifere/back-tt-22-2 && git checkout -b <nome_da_aula>
```

Substituindo `<nome_da_aula>` por middleware_vallidation por exemplo.
Sendo assim, tudo o que você for modificar no código puxado, você deve subir na sua branch específica, para que tenhamos uma ótima organização do repositório.

Caso queria haja necessidade de utilizar o código de outra aula, deve-se dar um git clone específico da branch desejada com o seguinte comando:
```git
git clone -b <nome_da_branch https://gitlab.com/lumifere/back-tt-22-2.git
```

<br>
<hr>

## :coffin: Execução

Após baixar os arquivos de uma branch, rode os seguintes comandos:

```bash
cd back-tt-22-2
npm install
```

Após isso, todos os testes devem ser realizados no Postman ou a ferramenta que o instrutor preferir, de forma a evidenciar o funcionamento do código utilizado durante a aula.

<br>
<hr>

## :soccer: Elenco

|         | 
| ---                |
| Guilherme Matera   |
| Christian Oliveira |
| Cristina Souza     |
| Nicholas Araujo    |
| Pedro Mateus       |
| Rayssa Gomes       |
| Renato Longo       |
| Wesley Conceição   |

<hr>
</div>
